<?php
namespace App\Http\Controllers;


use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Get the main view for the page.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Recursively get all sub accounts for the main enom account.
     *
     * @param $accounts
     * @param int $position
     */
    protected function getSubAccounts(&$accounts, $position = 0)
    {
        $ch = curl_init("https://reseller.enom.com/interface.asp?command=getsubaccounts&uid=gdsi&pw=j*nQ1m5F&ResponseType=XML&StartPosition={$position}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
        $data = json_decode(json_encode($data), true);
        $accounts = array_merge($accounts, $data['SubAccounts']['SubAccount']);

        if ($data['SubAccounts']['EndPosition'] !== $data['SubAccounts']['Count']) {
            $this->getSubAccounts($accounts, $data['SubAccounts']['NextRecords']);
        }
    }

    /**
     * Get the sub accounts for the enom account.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function subAccounts()
    {
        $sub_accounts = [];

        $this->getSubAccounts($sub_accounts);

        return response()->json($sub_accounts, 200);
    }


    protected function getSubAccountDomains(&$domains, $accountId, $position = 1)
    {
        $ch = curl_init("https://reseller.enom.com/interface.asp?command=subaccountdomains&uid=gdsi&pw=j*nQ1m5F&account={$accountId}&tab=iown&ResponseType=XML&Start={$position}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
        $data = json_decode(json_encode($data), true);

        $domainList = [];
        if ($data['GetDomains']['DomainCount'] == 1) {
            $domainList[] = $data['GetDomains']['domain-list']['domain'];
        } else {
            foreach ($data['GetDomains']['domain-list']['domain'] as $domain){
                $domainList[] = $domain;
            }
        }
        $domains = array_merge($domains, $domainList);


        if ($data['GetDomains']['EndPosition'] !== $data['GetDomains']['DomainCount'] && $data['GetDomains']['DomainCount'] > 0) {
            $this->getSubAccountDomains($domains, $accountId, $data['GetDomains']['NextRecords']);
        }
    }

    /**
     * Get the domains for a sub account.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function subAccountDomains(Request $request, $id)
    {
        $domains = [];
        $this->getSubAccountDomains($domains, $id);

        return response()->json($domains, 200);
    }


    protected function getAccountDomains(&$domains, $position = 1)
    {
        $ch = curl_init("https://reseller.enom.com/interface.asp?command=advanceddomainsearch&uid=gdsi&pw=j*nQ1m5F&ResponseType=XML&StartPosition={$position}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
        $data = json_decode(json_encode($data), true);

        $domains = array_merge($domains, $data['DomainSearch']['Domains']['Domain']);

        if ($data['DomainSearch']['NextPosition'] > $data['DomainSearch']['StartPosition']) {
            $this->getAccountDomains($domains, $data['DomainSearch']['NextPosition']);
        }
    }

    /**
     * Get all domains tied to the main enom account
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function accountDomains()
    {
        $domains = [];
        $this->getAccountDomains($domains);

        return response()->json($domains, 200);
    }


    /**
     * Get the host records for the provided domain.
     *
     * @param Request $request
     * @param $tld
     * @param $sld
     * @return \Illuminate\Http\JsonResponse
     */
    public function domainHosts(Request $request, $tld, $sld)
    {
        $ch = curl_init("https://reseller.enom.com/interface.asp?command=gethosts&uid=gdsi&pw=j*nQ1m5F&sld={$sld}&tld={$tld}&ResponseType=XML");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);
        return response()->json($data, 200);
    }


    /**
     * Update the domain supplied with it's provided host records
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateHosts(Request $request)
    {
        $account = $request->json()->get('data');
        $ctr = 1;
        $query = [];
        foreach($account['hosts'] as $host) {
            $hostName = $host['name'];
            $recordType = $host['type'];
            $address = $host['address'];

            $query["hostname$ctr"] = $hostName;
            $query["recordtype$ctr"] = $recordType;

            if ($hostName === 'mail' && $address === '209.235.198.88'){
                $address = '162.217.170.90';
            } elseif ($recordType === 'TXT' && strpos($address, '209.235.198.80/27') !== false){
                $address = str_replace('209.235.198.80/27', '162.217.170.90', $address);
            }

            $query["address$ctr"] = $address;
            $ctr = $ctr + 1;
        }
        $hostQuery = http_build_query($query);
        $route = "https://reseller.enom.com/interface.asp?command=sethosts&uid=gdsi&pw=j*nQ1m5F&sld={$account['sld']}&tld={$account['tld']}&$hostQuery&ResponseType=XML";
         $ch = curl_init("https://reseller.enom.com/interface.asp?command=sethosts&uid=gdsi&pw=j*nQ1m5F&sld={$account['sld']}&tld={$account['tld']}&$hostQuery&ResponseType=XML");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        $data = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);

        return response()->json($data, 200);
    }

    public function test(Request $request)
    {
        var_dump($request->json()->get('data'));
        die;
    }
}